import React from 'react';
import MainTable from './components/MainTable';

export default class App extends React.Component {
  render() {
    return (
      <section className="container">
        <header className="d-flex justify-content-center py-3">
          <h3>Add Person</h3>
        </header>

        <main className="col-xs-12 col-md-10 offset-md-1 col-xl-8 offset-xl-2 py-3">
          <MainTable />
        </main>
      </section>
    );
  }
}
