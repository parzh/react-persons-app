import React from "react";
import InputForm from './InputForm';
import OutputTable from './OutputTable';

export default class MainTable extends React.Component {
	constructor(props) {
		super(props);
	
		this.state = {
			entries: []
		};
	}
	
	addEntry(entry) {
		this.setState({ entries: this.state.entries.concat(entry) });
	}

	render() {
		return (
			<div>
				<InputForm onSubmit={ this.addEntry.bind(this) } />
				<hr className="mx-5 my-4" />
				<OutputTable entries={ this.state.entries } />
			</div>
		);
	}
}