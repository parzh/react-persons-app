import React from "react";
import PropTypes from "prop-types";
import { AvForm, AvField, AvGroup } from "availity-reactstrap-validation";
import { Button, Dropdown, Label, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";

const rules = {
	firstName: {
		required: true,
		pattern: {
			value: /^[a-zа-яёґєії][a-zа-яёґєії'-]*/i,
			errorMessage: "First name contains incorrect characters!"
		},
		minLength: {
			value: 2,
			errorMessage: "First name is too short!"
		},
		maxLength: {
			value: 30,
			errorMessage: "First name is too long!"
		},
	},
	lastName: {
		required: true,
		pattern: {
			value: /^[a-zа-яёґєії][a-zа-яёґєії'-]*/i,
			errorMessage: "Last name contains incorrect characters!"
		},
		minLength: {
			value: 2,
			errorMessage: "Last name is too short!"
		},
		maxLength: {
			value: 30,
			errorMessage: "Last name is too long!"
		},
	},
	phone: {
		required: true,
		pattern: {
			value: /^(?:\+38)?(?:\(\d{3}\)|\d\(\d\d\)|\d{3})[-\s]*(?:\d[-\s]*){7}$/,
			errorMessage: `Phone must start either with "0" or "+380" and contain 10 or 12 digits`
		}
	},
	male: true,
	age: {
		required: true,
		min: {
			value: 5,
			errorMessage: "The age is too low!"
		},
		max: {
			value: 99,
			errorMessage: "The age is too high!"
		},
	}
};

const genders = [
	{
		label: "Male",
		value: true
	},
	{
		label: "Female",
		value: false
	}
];

export default class InputForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = this.baseState = {
			firstName: '',
			lastName: '',
			phone: '',
			male: true,
			age: '',
			dropdownOpen: false
		};
	}

	toggleDropdown() {
		this.setState({ dropdownOpen: !this.state.dropdownOpen });
	}

	reset() {
		this.setState(this.baseState);
	}

	handleSubmit(event, errors, values) {
		if (errors.length)
			return;

		const { male } = this.state;
		const data = Object.assign({}, values, { gender: male? "Male": "Female" });

		this.props.onSubmit(data);
		this.reset();
	}

	render() {
		return (
			<AvForm className="px-3" onSubmit={ this.handleSubmit.bind(this) }>
				<div className="row">
					<AvField
						groupAttrs={{ className: "col-sm-12 col-xl-6 px-1" }}
						name="firstName"
						value={ this.state.firstName }
						onChange={ ({ target }) => this.setState({ firstName: target.value }) }
						type="text"
						label="First Name"
						placeholder="John"
						validate={ rules.firstName }
					/>

					<AvField
						groupAttrs={{ className: "col-sm-12 col-xl-6 px-1" }}
						name="lastName"
						value={ this.state.lastName }
						onChange={ ({ target }) => this.setState({ lastName: target.value }) }
						type="text"
						label="Last Name"
						placeholder="Doe"
						validate={ rules.lastName }
					/>
				</div>

				<div className="row">
					<AvGroup className="col-sm-12 mt-xl-3 px-0 align-items-center">
						<Label className="mr-2 ml-1 my-1">Gender</Label>

						<Dropdown
							className="d-inline"
							isOpen={ this.state.dropdownOpen }
							toggle={ () => this.toggleDropdown() }
						>
							<DropdownToggle color="light" caret>
								{ this.state.male? "Male" : "Female" }
							</DropdownToggle>

							<DropdownMenu>
								{ genders.map(({ label, value }) =>
									<DropdownItem key={ label } onClick={ () => this.setState({ male: value }) }>{ label }</DropdownItem>
								) }
							</DropdownMenu>
						</Dropdown>
					</AvGroup>
				</div>

				<div className="row">
					<AvField
						groupAttrs={{ className: "col col-sm-10 px-1" }}
						name="phone"
						value={ this.state.phone }
						onChange={ ({ target }) => this.setState({ phone: target.value }) }
						type="text"
						label="Phone"
						placeholder="+38 (098) 123-45-67"
						validate={ rules.phone }
					/>

					<AvField
						groupAttrs={{ className: "col col-sm-2 px-1" }}
						name="age"
						value={ this.state.age }
						onChange={ ({ target }) => this.setState({ age: +target.value }) }
						type="number"
						label="Age"
						placeholder="5+"
						validate={ rules.age }
					/>
				</div>

				<div className="row">
					<Button className="col-sm-12 col-md-6 px-1" color="primary" type="submit">Add</Button>
					<Button className="col-sm-12 col-md-6 px-1" color="light" type="reset" onClick={ () => this.reset() }>Reset</Button>
				</div>
			</AvForm>
		);
	}
}

InputForm.propTypes = {
	onSubmit: PropTypes.func.isRequired
};
