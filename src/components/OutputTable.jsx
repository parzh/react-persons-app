import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import "react-table/react-table.css";

const columns = [
	{ Header: "First Name", accessor: "firstName" },
	{ Header: "Last Name", accessor: "lastName" },
	{ Header: "Gender", accessor: "gender" },
	{ Header: "Phone", accessor: "phone" },
	{ Header: "Age", accessor: "age" }
];

export default class OutputTable extends React.Component {
	render() {
		return (
			<ReactTable
				className="-striped"
				data={ this.props.entries }
				columns={ columns }
				defaultPageSize={ 10 }
				minRows={ 1 }
				showPagination={ false }
			/>
		);
	}
}

OutputTable.propTypes = {
	entries: PropTypes.arrayOf(PropTypes.object).isRequired
}